"""
Copyright © 2023 Arm Ltd and Contributors. All rights reserved.
SPDX-License-Identifier: Apache-2.0
"""
import argparse

def ArgConf():
    """
    Parse command line arguments.

    Returns:
        dict: A dictionary containing parsed command line arguments.
              Keys: "port", "model", "cpu_cores"
    """
    
    parser = argparse.ArgumentParser(description="Parse command line arguments.")
    parser.add_argument("-p", "--port", required=True, help="Port number (not 5000)")
    parser.add_argument("-m", "--model", required=True, help="Model to be used (tiny_yolov3/yolov3)")
    parser.add_argument("-c", "--cpu_cores", default="4", required=False, help="CPU cores on the device")

    args = vars(parser.parse_args())

    return args
